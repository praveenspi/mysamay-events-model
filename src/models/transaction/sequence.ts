export class Sequence {
    _id: string = null;
    sequence: number = null;
    prefix: string = null;

    static collectionName = "sequence"
}