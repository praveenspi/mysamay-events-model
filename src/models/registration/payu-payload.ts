export class PayUPayload {
    key: string;
    txnid: string;
    amount: string;
    productinfo: string;
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    address1?: string;
    adress2?: string;
    city?: string;
    state?: string;
    country?: string;
    zipcode?: string;
    /**
     * Success redirect URL
     */
    surl: string;
    /**
     * Failure redirect URL
     */
    furl: string;
    hash: string;
    /**
     * creditcard|debitcard
     */
    drop_category?: string;
}