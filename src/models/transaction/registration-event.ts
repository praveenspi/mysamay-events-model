import { EventCategory } from './../events/event-category';
import { EventRegistration } from './../registration/event-registration';
import { RegistrationEventType } from './../../enums/registration-event-type';
import { MetadataHelper } from '@neb-sports/mysamay-common-utils';

var dateFields = [
    "eventTime"
]

/**
 * Immutable registration event. It will be used to 
 * capture all the user events during an event registration.
 */
export class RegistrationEvent {
    _id: string = null;
    eventTime: Date = null;
    eventType: RegistrationEventType = null;
    eventId: string = null;
    txnid: string = null;
    eventCategory: EventCategory = null;
    registration: EventRegistration = null;
    payuData: any = null;

    constructor(data: Partial<RegistrationEvent>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Events_Transactions"
    
}