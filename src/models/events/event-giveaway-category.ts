export class EventGiveawayCategory {

    label: string = null;
    lowerLimit: number = null;
    upperLimit: number = null;

    constructor(data: Partial<EventGiveawayCategory>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                this[key] = data[key];
            }
            else {
                delete this[key];
            }
        });
    }

}