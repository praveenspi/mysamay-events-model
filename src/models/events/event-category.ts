import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

var dateFields = [
    "regStartDate", 
    "regEndDate",
    "startDate"
];

export class EventCategory {
    _id: string = null;
    offerDescription: string = null;
    offerLabel: string = null;
    regFee: number = null;
    regStartDate: Date = null;
    regEndDate: Date = null;
    bannerCaption: string = null;
    name: string = null;
    targetDistance: number = null;
    regType: string = null;
    startDate: Date = null;
    startTime: string = null;
    gstPercentage: number = null;
    gstIncluded: boolean = true;
    routeMap: string = null;
    feeAfterDiscount: number = null;
    finalAmount: number = null;
    gstAmount: number = null;

    constructor(data: Partial<EventCategory>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Events_EventData";
}