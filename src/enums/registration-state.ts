export enum RegistrationState {
    /**
     * User has started registration process.
     * He is now in the registration form page.
     * Saved as draft.
     * 
     * Order = 1
     */
    INITIATED = "INITIATED",
    /**
     * User has filled all the required fields and submitted 
     * the regstration form.
     * 
     * Order = 2
     */
    FORM_SUBMITTED = "FORM_SUBMITTED",
    /**
     * User has now been redirected to payment gateway.
     * 
     * Order = 3
     */
    PAYMENT_INITIATED = "PAYMENT_INITIATED",

    /**
     * Order = 4
     */
    PAYMENT_FAILED = "PAYMENT_FAILED",

    /**
     * Order = 5
     */
    PAYMENT_SUCCESS = "PAYMENT_SUCCESS",

    /**
     * Order = 6
     */
    REGISTRATION_SUCCESS = "REGISTRATION_SUCCESS",

    REFUND_INITIATED = "REFUND_INITIATED",
    REFUND_SUCCESS = "REFUND_SUCCESS"
}

var order = new Map<string, number>();
order.set("INITIATED", 1)
order.set("FORM_SUBMITTED", 2)
order.set("PAYMENT_INITIATED", 3)
order.set("PAYMENT_FAILED", 4)
order.set("PAYMENT_SUCCESS", 5)
order.set("REGISTRATION_SUCCESS", 6)
export {order};


