import { MetadataHelper } from '@neb-sports/mysamay-common-utils';

const dateFields = ["createdTime", "updatedTime", "displayFrom", "displayTo"]

export class BannerConfig {
    _id: string = null;
    url: string = null
    eventUrl: string = null;
    eventId: string = null;
    displayFrom: Date = null;
    displayTo: Date = null;
    createdTime: Date = null;
    updatedTime: Date = null;

    constructor(data: Partial<BannerConfig>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Events_BannerConfigs"
}