import { RegistrationFieldSetup } from './../models/registration/registration-field-setup';
import { DataType } from './../enums/data-type';
import { RegCustomField } from '../models/registration/custom-field';
export class CustomFieldHelper {


    static validateCustomField(customFields: RegCustomField[]): string {
        for(let customField of customFields) {
            let dataType = customField.dataType;
            switch(dataType) {
                case DataType.STRING:
                    if(!(typeof customField.value === "string")) {
                        return `invalid number value [${customField.value}] for field [${customField.fieldKey}]`
                    }
                break;
                case DataType.NUMBER:
                    if(!(typeof customField.value === "number")) {
                        return `invalid number value [${customField.value}] for field [${customField.fieldKey}]`
                    }
                break;
                case DataType.DATE:
                    let dateValue = new Date(customField.value)
                    if(!dateValue) {
                        return `invalid date value [${customField.value}] for field [${customField.fieldKey}]`
                    }
                break;
                default:
                    return `unknown datatype for field ${customField.fieldKey}. only string, numner or Date is allowed.`
            }
        }
        return null;
    }

    static validateRegistrationField(regFields: RegistrationFieldSetup[]): string {
        for(let regField of regFields) {
            if(!Object.values(DataType).includes(regField.dataType)) {
                return `unknown data type ${regField.dataType}`
            }
        }
        return null;
    }
}