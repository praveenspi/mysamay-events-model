export interface Registration {
    offer_description: string;
    offer_label: string;
    reg_fee: number;
    end_date: Date;
    start_date: Date;
    banner_caption: string;
    name: string;
    _id: string;
    reg_type: string;
}

export interface CustomField {
    is_required: boolean;
    is_displayed: boolean;
    label: string;
    fieldname: string;
    _id: string;
}

export interface RegSchema {
    is_required: boolean;
    is_displayed: boolean;
    fieldname: string;
    _id: string;
}

export interface Category {
    max_time_limit: string;
    start_time: string;
    start_date: Date;
    age_limit_minimum: number;
    name: string;
    _id: string;
    registration: Registration[];
    customFields: CustomField[];
    reg_schema: RegSchema[];
}

export interface Faq {
    q: string;
    a: string;
    _id: string;
}

export interface ExternalEvent {
    _id: string;
    attendee_limit: string;
    venue: string;
    registration_close: Date;
    registration_open: Date;
    event_type: string;
    event_website: string;
    organiser_website: string;
    event_date: Date;
    about_charity_partners: string;
    about_title_sponsors: string;
    about_organisers: string;
    description_marathon: string;
    description_run: string;
    promo_video_url: string;
    route: string;
    banner_url: string;
    logo_url: string;
    list_on_home_screen: boolean;
    name: string;
    organiser_support_phone: string;
    organiser_support_email: string;
    organiser_reg_platform: string;
    organiser_address: string;
    organiser_email: string;
    organiser_name: string;
    company_address: string;
    company_email: string;
    company_name: string;
    event_id: string;
    categories: Category[];
    faq: Faq[];
    notes: any[];
    stage: string;
    published_status: string;
    __v: number;
    banner_number: number;
    map_url?: any;
    tab_4_caption?: any;
    tab_4_value?: any;
    tab_5_caption?: any;
    tab_5_value?: any;
    tab_6_caption?: any;
    tab_6_value?: any;
    eventStartDate: Date;
    eventEndDate: Date;
    guidelines: string[];
    customLeaderboard: boolean;
    dailyLimit: number;
    dailyMinLimit: number;
    eventCategory: string;
    
}