import { EventEntity } from '../models/events/event-entity';
import { ExternalEvent } from '../interfaces/external-event';
import { EventCategory } from "../models/events/event-category";
export class EventConverter {
    static convertFromExternalEvent(externalEvent: ExternalEvent) {
        let categories: EventCategory[] = [];
        externalEvent.categories.forEach(cat => {
            
            let targetDistance = 10;
            switch(cat.name) {
                case "5K":
                    targetDistance = 5;
                    break;
                case "10K":
                    targetDistance = 10;
                    break;
                case "Half Marathon":
                    targetDistance = 21.1;
                    break;
                case "Marathon":
                    targetDistance = 42.2;
                    break;
            }   
            let data = {
                name: cat.name,
                targetDistance: targetDistance,
                startDate: cat.start_date,
                startTime: cat.start_time
            }
            categories.push(new EventCategory(data));
        });
        let data: Partial<EventEntity> = {
            attendeeLimit: parseInt(externalEvent.attendee_limit),
            venue: externalEvent.venue,
            bannerUrl: externalEvent.banner_url,
            categories: categories,
            companyAddress: externalEvent.company_address,
            companyEmail: externalEvent.company_email,
            companyName: externalEvent.company_name,
            description: externalEvent.description_marathon || externalEvent.description_run,
            eventDate: externalEvent.event_date,
            eventType: externalEvent.event_type,
            eventWebsite: externalEvent.event_website,
            externalEventId: externalEvent.event_id,
            logoUrl: externalEvent.logo_url,
            name: externalEvent.name,
            organiserAddress: externalEvent.organiser_address,
            organiserEmail: externalEvent.organiser_email,
            organiserName: externalEvent.organiser_name,
            organiserRegPlatform: externalEvent.organiser_reg_platform,
            organiserSupportEmail: externalEvent.organiser_support_email,
            organiserSupportPhone: externalEvent.organiser_support_phone,
            organiserWebsite: externalEvent.organiser_website,
            registrationClose: new Date(externalEvent.registration_close),
            registrationOpen: new Date(externalEvent.registration_open),
            guidelines: externalEvent.guidelines,

        }
        return new EventEntity(data);
    }
}