import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

var dateFields = [
    "invoiceDate"
]
export class GSTReport {
    _id: string = null;
    invoiceType: "B2C" | "B2B" = null;
    gstin: string = null;
    regFee: number = null;
    invoiceNumber: string = null;
    invoiceAmount: number = null;
    invoiceDate: Date = null;
    quantity: number = null;
    cancelled: "Yes" | "No" = null;
    taxType: string = null;
    taxEvent: string = null;
    taxRate: number = null;
    taxAmount: number = null;
    eventId: string = null;
    eventName: string = null;
    year: number = null;
    month: number = null;
    /**
     * tag for quick monthly search
     * 
     * values can be Aug-22, Jun-23 etc
     */
    tag: string = null;

    constructor(data: Partial<GSTReport>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
    }

    static collectionName = "Reports_GSTReport"
}