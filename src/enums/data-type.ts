export enum DataType {
    STRING = "String",
    DATE = "Date",
    NUMBER = "Number",
    BOOLEAN = "Boolean",
    SELECT = "Select",
    MULTI_SELECT = "MultiSelect"
}