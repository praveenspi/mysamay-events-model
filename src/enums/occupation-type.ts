export enum OccupationType {
    //Business, Self- employed, salaried, retired, house wife, student, Govt., Other
    BUSINESS = "Business",
    SELF_EMPLOYED = "Self Employed",
    SALARIED = "Salaried",
    RETIRED = "Retired",
    HOUSE_WIFE = "House Wife",
    STUDENT = "Student",
    GOVT = "Govt",
    OTHERS = "OTHERS"
}