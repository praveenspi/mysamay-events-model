import { DataType } from '../../enums/data-type';

export class RegCustomField {
    label: string = null;
    fieldKey: string = null;
    dataType: DataType = null;
    value: any = null;

    constructor(data: Partial<RegCustomField>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                switch(data.dataType) {
                    case DataType.DATE:
                        this[key] = new Date(data[key])
                    default:
                        this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
    }
}
