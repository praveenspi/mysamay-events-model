export enum BloodGroup {
    //O+, O-, A+, A-, B+, B-, AB+, AB-
    O_POSITIVE = "O+",
    O_NEGATIVE = "O-",
    A_POSITIVE = "A+",
    A_NEGATIVE = "A-",
    B_POSITIVE = "B+",
    B_NEGATIVE = "B-",
    AB_POSITIVE = "AB+",
    AB_NEGATIVE = "AB-"
}