export enum IDType {
    PAN_CARD = "PAN Card",
    ADHAAR_CARD = "Adhaar Card",
    VOTER_ID = "Voter ID",
    PASSPORT = "Passport",
    DRIVING_LICENSE = "Driving License",
    OTHERS = "Others"
}