import { OccupationType } from './../../enums/occupation-type';
export class Occupation {
    occupationType: OccupationType = null;
    organization: string = null;
    designation: string = null;
}