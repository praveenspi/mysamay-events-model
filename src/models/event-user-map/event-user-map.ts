import { MetadataHelper } from "@neb-sports/mysamay-common-utils";
var dateFields = [
    "createdTime",
    "updatedTime"
]
export class EventUserMap {
    _id: string = null;
    userId: string = null;
    eventId: string = null;
    createdTime: Date = null;
    updatedTime: Date = null;


    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if (!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Events_EventUserMap"
}
