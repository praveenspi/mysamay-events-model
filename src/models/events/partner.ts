import { Address } from "../registration/address";

export class Partner {
    name: string = null;
    logoUrl: string = null;
    address: Address = null;
    email: string = null;
    supportEmail: string = null;
    supportPhone: string = null;
    partnerType: string = null;
}