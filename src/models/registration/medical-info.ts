import { BloodGroup } from './../../enums/blood-group';
export class MedicalInfo {
    bloodGroup: BloodGroup = null;
    chronicDisease: boolean = false;
    otherDisorders: boolean = false;
    medicalCondition: string = null;
}