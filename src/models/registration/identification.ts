import { IDType } from './../../enums/id-type';
export class Identification {
    idType: IDType = null;
    docURL: string = null;
    valid: boolean = true;
}