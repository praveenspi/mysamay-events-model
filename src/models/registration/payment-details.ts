export class PaymentDetail {
    payuRefNum: string = null;
    fee: number = null;
    discount: number = null;
    gst: number = null;
    finalAmount: number = null;
    payuHash: string = null;
    reverseHash: string = null;
    keyString: string = null;
    reverseKeyString: string = null;
    failedReason: string = null;
    paidOn: Date = null;
}