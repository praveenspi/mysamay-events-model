import { MetadataHelper } from '@neb-sports/mysamay-common-utils';
import { DataType } from './../../enums/data-type';
var dateFields = [
    "createdTime",
    "updatedTime"
]
export class RegistrationFieldSetup {
    _id: string = null;
    fieldKey: string = null;
    label: string = null;
    tooltip: string = null;
    placeHolder: string = null;
    description: string = null;
    dataType: DataType = null;
    textArea: boolean = false;
    required: boolean = true;
    inputValues: string[] = null;
    createdTime: Date = null;
    updatedTime: Date = null;
    

    constructor(data: Partial<RegistrationFieldSetup>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if (!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if (!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    static collectionName = "Events_RegistrationFields"
}
