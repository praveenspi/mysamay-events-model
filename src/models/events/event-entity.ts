import { RegistrationFieldSetup } from './../registration/registration-field-setup';
import { EventGiveawayCategory } from './event-giveaway-category';
import { MetadataHelper } from "@neb-sports/mysamay-common-utils";
import { EventCategory } from "./event-category";
import { Partner } from './partner';

var dateFields = [
    "createdTime", 
    "updatedTime",
    "registrationOpen",
    "registrationClose",
    "eventDate",
    "eventStartDate",
    "eventEndDate"
];

export class EventEntity {
    _id: string = null;
    externalEventId: string = null;
    city: string = null;
    venue: string = null;
    attendeeLimit: number = null;
    registrationOpen: Date = null;
    registrationClose: Date = null;
    eventType: string = null;
    eventWebsite: string = null;
    organiserWebsite: string = null;
    eventDate: Date = null;
    eventStartDate: Date = null;
    eventEndDate: Date = null;
    description: string = null;
    bannerUrl: string = null;
    logoUrl: string = null;
    name: string = null;
    dailyLimit: number = null;
    dailyMinLimit: number = null;
    eventCategory: string = null;
    customLeaderboard: boolean = false;
    guidelines: string[] = ["Have a great run."];
    organiserSupportPhone: string = null;
    organiserSupportEmail: string = null;
    organiserRegPlatform: string = null;
    organiserAddress: string = null;
    organiserEmail: string = null;
    organiserName: string = null;
    companyName: string = null;
    companyAddress: string = null;
    companyEmail: string = null;
    registrationStartDate: Date = null;
    registrationEnddate: Date = null;

    
    categories: EventCategory[] = null;
    eventGiveawayCategories: EventGiveawayCategory[] = null;
    finalized: boolean = false;
    manualEntry: boolean = true;
    paceThreshold: number = null;
    promotionEnabled: boolean = true;
    createdTime: Date = null;
    updatedTime: Date = null;
    targetDistance: number = null;
    
    // phase5
    tagLine: string = null;
    companyLogo: string = null;
    companyWebsite: string = null;
    organizerLogo: string = null;
    circuitEventId: string = null;
    circuitEvents: string[] = null;
    virtual: boolean = true;
    eventLocation: string = null;

    partners: Partner[] = null;
    sponsors: Partner[] = null;

    registrationFields: RegistrationFieldSetup[] = null;


    constructor(data: Partial<EventEntity>) {
        Object.keys(this).forEach(key => {
            if (key in data) {
                if(key === "categories") {
                    this.categories = data[key].map((cat: any) => new EventCategory(cat));
                }
                else if (dateFields.includes(key)) {
                    this[key] = new Date(data[key]);
                }
                else if(key === "registrationFields") {
                    this[key] = data.registrationFields.map(rf => new RegistrationFieldSetup(rf))
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.createdTime) {
            this.createdTime = new Date();
        }
        this.updatedTime = new Date();
    }

    /**
     * Check if this object contains key.
     * @param key
     */
    containsKey(key: string): boolean {
        return Object.keys(this).includes(key);
    }

    static collectionName = "Events_EventData";
}